import {bootstrap, Component, CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';
import {Decoration} from "./decoration.ts";

@Component({
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    selector: 'christmas-tree',
    template: ` 
      <div class="col-lg-6">
        <div class="tree">
          <div class="branches">
            <div class="branch top"></div>
            <div class="branch middle"></div>
            <div class="branch bottom"></div>
          </div>
          <div class="root"></div>
        </div>
        <div class="decorations">
          <div 
          *ng-for="#decoration of decorations; #i = index" 
          class="decoration item-{{i}}"
          [ng-style]="{
              'background-color': decoration.color,
              'width': decoration.size,
              'margin-top': decoration.marginTop,
              'margin-left': decoration.marginleft,
              'height': decoration.size}"
          (click)="selectedDecoration = decoration">
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="panel panel-default decoration-form">
          <div class="panel-heading">
            Edycja bombki
          </div>
          <div class="panel-body">
            <div class="form-group">
              <div class="input-group input-group-sm">
                <span class="input-group-addon">Kolor</span>
                <input type="color" class="form-control" [(ng-model)]="selectedDecoration.color" />
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-sm">
                <span class="input-group-addon">Rozmiar</span>
                <input type="range" min="0" max="50" class="form-control" [(ng-model)]="selectedDecoration.size" />
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-sm">
                <span class="input-group-addon">Margines górny</span>
                <input type="number" class="form-control" [(ng-model)]="selectedDecoration.marginTop" />
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-sm">
                <span class="input-group-addon">Margines lewy</span>
                <input type="number" class="form-control" [(ng-model)]="selectedDecoration.marginleft" />
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <small>*zaznacz bombkę, aby zacząć ją edytować</small>
          </div>
        </div>
      </div>
    `
})
class ChristmasTree {
    public decorations: Decoration[] = [
        new Decoration('#ffff09', -350, 344, 26),
        new Decoration('#808080', 33, 326, 30),
        new Decoration('#04455e', 31, 391, 29),
        new Decoration('#ff8080', -33, 265, 28),
        new Decoration('#97004b', 14, 322, 34),
        new Decoration('#ff8000', 26, 430, 28),
        new Decoration('#0080c0', -21, 227, 28),
        new Decoration('#b30000', -21, 339, 24)
    ];
    public selectedDecoration: Decoration= this.decorations[0];
 }
bootstrap(ChristmasTree);